package com.chinoSan.Full.stack.app.java.react.repository;

import com.chinoSan.Full.stack.app.java.react.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends JpaRepository<Student, Integer> {
}
