package com.chinoSan.Full.stack.app.java.react.service;

import com.chinoSan.Full.stack.app.java.react.model.Student;

import java.util.List;

public interface StudentService {
    public Student saveStudent(Student student);
    public List<Student> getAllStudents();

}
