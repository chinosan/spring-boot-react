# Full stack app - Integrate spring boot & react

_Simple CRUD with Spring-boot-React-MySql_

![List items](./screenshots/image1.png "MarineGEO logo")
## Setup 🚀
_This instructions let you get a functional copy in you local machine for develop and test._

just open on intellij IDEA and change application.properties
for the corresponding to your enviroment



### Tools 📋

_What technologies are used in this project_

```
Material UI
Mysql
Vite
Spring JPA
```

## Ejecutando las pruebas ⚙️

_Explica como ejecutar las pruebas automatizadas para este sistema_

### Analice las pruebas end-to-end 🔩

_Explica que verifican estas pruebas y por qué_

```
Da un ejemplo
```

### Y las pruebas de estilo de codificación ⌨️

_Explica que verifican estas pruebas y por qué_

```
Da un ejemplo
```

